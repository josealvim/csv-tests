.PHONY: run clean

SRCS  = $(shell find -type f -name "*.cpp")
OBJS  = $(patsubst %.cpp, %.o, $(SRCS))
FLAG ?= -g -std=c++20 -I.
CXX  ?= g++

run: tests.out
	./tests.out

clean:
	find -type f -name "*.out" -exec rm -rf {} \;
	find -type f -name "*.o"   -exec rm -rf {} \;

tests.out: $(OBJS)
	$(CXX) $(FLAG) -o $@ $^

%.o: %.cpp
	$(CXX) $(FLAG) -c -o $@ $<
