#include <csv/csv.hpp>

#include <sstream>
#include <iostream>
#include <functional>
#include <vector>

int main () {
	size_t	total_tests    = 0,
		total_failures = 0;

	auto run_unit_test = [&](
		std::string_view name, 
		size_t pad_to, 
		auto&& test
	){
		total_tests++;
		std::cout
			<< name 
			<< std::string(pad_to - name.size(), '.')
			<< ": ";
		if (test()) {
			std::cout << "\33[32mpassed\33[0m" << std::endl;
		} else {
			std::cout << "\33[31mfailed\33[0m" << std::endl;
		}
	};

	auto print_record = [](csv::record_t const& record) {
		std::cout << std::endl << "> ";
		for (auto& field : record)
			std::cout << "“" << field << "”" << " ";
		std::cout << std::endl;
	};

	struct UnitTest {
		std::string name;
		std::function<bool()> check;
	};

	std::vector<UnitTest> tests = {
	{"reading record from istream&",
	[&](){
		std::stringstream ss("");
		std::istream& is = ss;
		csv::read_record(is);
		return true;
	}},
	{"record read from empty stream is empty",
	[&](){
		std::stringstream ss("");
		return csv::read_record(ss).size() == 0;
	}},
	{"reading from \"a,b,c\" yields three fields",
	[&](){
		std::string test_string = "a,b,c";
		std::stringstream ss(test_string);
		auto record = csv::read_record(ss);
		return record.size() == 3;
	}},
	{"reading from \"a,b,c\" yields {\"a\",\"b\",\"c\"}",
	[&](){
		std::string test_string = "a,b,c";
		std::stringstream ss(test_string);
		auto record = csv::read_record(ss);
		return record == csv::record_t{
			"a", "b", "c"
		};
	}},
	{"reading fields than one character long",
	[&](){
		std::string test_string = "foobar,asdas,goo, ";
		std::stringstream ss(test_string);
		auto record = csv::read_record(ss);
		return record == csv::record_t{
			"foobar", "asdas", "goo", " "
		};
	}},
	{"reading empty fields is okay",
	[&](){
		std::string test_string = ",fl,,ab";
		std::stringstream ss(test_string);
		auto record = csv::read_record(ss);
		return record == csv::record_t{
			"","fl","","ab"
		};
	}},
	{"if the last field is empty, it is empty!",
	[&](){
		std::string test_string = "aaa,bbb,ccc,";
		std::stringstream ss(test_string);
		auto record = csv::read_record(ss);
		return record == csv::record_t{
			"aaa","bbb","ccc",""
		};
	}},
	{"whitespace isn't ignored",
	[&](){
		std::string test_string = "  ,a,\t\t,\r,  ";
		std::stringstream ss(test_string);
		auto record = csv::read_record(ss);
		return record == csv::record_t{
			"  ","a","\t\t","\r","  "
		};
	}},
	{"fields may be surrounded by quotes",
	[&](){
		std::string quoted = "abc,\"foobar\",cdf";
		std::stringstream quoted_ss(quoted);
		
		std::string normal = "abc,foobar,cdf";
		std::stringstream normal_ss(normal);

		auto quoted_record = csv::read_record(quoted_ss);
		auto normal_record = csv::read_record(normal_ss);

		return quoted_record == normal_record;
	}},
	{"quotes around the last field",
	[&](){
		std::string test_string = "a,b,\"c\"";
		std::stringstream ss(test_string);
		auto record = csv::read_record(ss);
		return record == csv::record_t{"a","b","c"};
	}},
	{"can escape quotes inside quoted field",
	[&](){
		std::string test_string = 
			"abc,\"\"\"fo\"\"o\"\"bar\"\"\",cdf";
		std::stringstream ss(test_string);
		auto record = csv::read_record(ss);
		return record == 
			csv::record_t {"abc","\"fo\"o\"bar\"","cdf"};
	}},
	{"empty string is treated as an empty record",
	[&](){
		std::string test_string = ",\"\",asdf, ,fdsa";
		std::stringstream ss(test_string);
		auto record = csv::read_record(ss);
		return record  ==
			csv::record_t {"","","asdf"," ","fdsa"};
	}},
	{"leading and trailing characters around quoted field are ignored",
	[&](){
		std::string test_string = 
			"  \"\" , \"aa\",\" bb \" , \"fo\"\"\"\"obar\"  ";
		std::stringstream ss(test_string);
		auto record = csv::read_record(ss);
		return record ==
			csv::record_t {"","aa"," bb ","fo\"\"obar"};
	}},
	{"last field may end with a newline",
	[&](){
		std::string test_string = "a,b,c\n";
		std::stringstream ss(test_string);
		auto record = csv::read_record(ss);

		return record == csv::record_t{"a","b","c"};
	}},
	{"reading multiple records separated by LF in sequence",
	[&](){
		std::string test_string = "a,b,c\nd,e,f";
		std::stringstream ss(test_string);
		
		auto record00 = csv::read_record(ss);
		auto record01 = csv::read_record(ss);

		return  record00 == csv::record_t{"a","b","c"}
		and	record01 == csv::record_t{"d","e","f"};
	}},
	{"reading multiple records separated by CRLF in sequence",
	[&](){
		std::string test_string = "a,b,c\r\nd,e,f";
		std::stringstream ss(test_string);
		
		auto record00 = csv::read_record(ss);
		auto record01 = csv::read_record(ss);
				
		return  record00 == csv::record_t{"a","b","c"}
		and	record01 == csv::record_t{"d","e","f"};
	}},
	{"newlines and carriage returns in strings are preserved",
	[&](){
		std::string test_string = "a,\"foo\nbar\",\"fiz\r\nbuz\",\"b\n\"";
		std::stringstream ss(test_string);
		auto record = csv::read_record(ss);
		return record == 
			csv::record_t{"a","foo\nbar","fiz\r\nbuz","b\n"};
	}},
	{"commas are preserved within strings",
	[&](){
		std::string test_string = 
			"\",\",\"asd,\"\"a\"\"\",\"a,b\"";
		std::stringstream ss(test_string);
		auto record = csv::read_record(ss);
		return record == 
			csv::record_t{",","asd,\"a\"","a,b"};
	}},
	{"can iterate over record_reader_t",
	[&](){
		std::string test_string = "a,b,c";
		std::stringstream ss(test_string);
		for (auto&& record : csv::record_reader_t(ss)) {
			(void) record;
		}
		return true;
	}},
	{"ignore LF on the last line",
	[&](){
		std::string test_string = 
			"a,b,c\n"
			"1,2,3\n"
			"\"hello, world!\",\"I can even\n"
			"_break_ lines\",:)\n"
			"O,o,O\n";

		std::stringstream ss(test_string);
		size_t num_lines = 0;
		for (auto&& record : csv::record_reader_t(ss)) {
			num_lines++;
		}
		return num_lines == 4;
	}},
	{"ignore CRLF on the last line",
	[&](){
		std::string test_string = 
			"a,b,c\n"
			"1,2,3\n"
			"\"hello, world!\",\"I can even\n"
			"_break_ lines\",:)\n"
			"O,o,O\r\n";

		std::stringstream ss(test_string);
		size_t num_lines = 0;
		for (auto&& record : csv::record_reader_t(ss)) {
			num_lines++;
		}
		return num_lines == 4;
	}},
	{"iterating over the records yields the expected sequence",
	[&](){
		std::string test_string = 
			"a,b,c\n"
			"1,2,3\r\n"
			"\"hello, world!\",\"I can even\n"
			"_break_ lines\",:)\n"
			"O,o,O\n";
		std::vector<csv::record_t> records = {
			{"a","b","c"},
			{"1","2","3"},
			{"hello, world!","I can even\n_break_ lines",":)"},
			{"O","o","O"}
		};
		size_t i = 0;

		std::stringstream ss(test_string);
		for (auto&& record : csv::record_reader_t(ss)) {
			if (records[i++] != record)
				return false;
			else
				continue;
		}
		return true;
	}},
	{"reading mapped, as opposed to raw records",
	[&](){
		std::string test_string = 
			"a,b,c\n"
			"1,2,3\r\n"
			"\"hello, world!\",\"I can even\n"
			"_break_ lines\",:)\n"
			"O,o,O\n";
		std::stringstream ss(test_string);
		std::vector<csv::mapped_t> maps = {
			{
				{"a","1"},
				{"b","2"},
				{"c","3"}
			},
			{
				{"a","hello, world!"},
				{"b","I can even\n_break_ lines"},
				{"c",":)"}
			},
			{
				{"a","O"},
				{"b","o"},
				{"c","O"}
			}
		};

		size_t i = 0;
		auto reader = csv::mapped_reader_t(ss);
		for (auto map : reader) {try {
			bool ok = (map.at("a") == maps[i].at("a")) 
				&&(map.at("b") == maps[i].at("b")) 
				&&(map.at("c") == maps[i].at("c")); 
			i++;
			if (not ok)
				return false;
		} catch (csv::mapped_iterator::SizeMismatchError) {
			return false;
		}}
		return true;
	}}
	};

	std::string header = " josealvim's libcsv unit tests ";
	size_t max_name_len = header.size();
	for (auto const& test : tests) {
		max_name_len = max_name_len < test.name.size()? 
			test.name.size(): max_name_len;
	}
	auto pad = max_name_len + sizeof(": passed") - header.size() - 1;
	std::cout
		<< std::endl	
		<< std::string(pad/2, '-') 
		<< header
		<< std::string(pad/2+pad%2, '-')
		<< std::endl	
		<< std::endl;

	for (auto const& test : tests) 
		run_unit_test(test.name, max_name_len, test.check);

	return 0;
}
